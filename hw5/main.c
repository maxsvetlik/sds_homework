#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#define B(i,j) b[(i) + (j)*n]
#define C(i,j) c[(i) + (j)*n]
#define IDENT(i,j) ident[(i) + (j)*n]
#define bcopy(i,j) bcopy[(i) + (j)*n]
void dgemm_();
void dgsv_();
int limit = 7;
int n = 8;

double bFunc(double in){
	return pow(in,7);
}
double* getBMat(){
	double *bcopy = malloc(sizeof(double)*n*n);
	double *b = malloc(sizeof(double)*n*n);
	double *ident = malloc(sizeof(double)*n*n);
	int i,j;
	double val;
	for(i = 0; i < n; ++i){
		for(j = 0; j < n; ++j){
			val = ((double)i/(double)limit);
			B(i,j) = pow(val,(double)j);	
			bcopy(i,j) = pow(val,(double)j);
			printf("%16.6g ", B(i,j));
			if(i == j)
				IDENT(i,j) = 1;
			else
				IDENT(i,j) = 0;
		}
		printf("\n");
	}
	printf("\nB inverse:\n");
	int info;
	int *ipiv=malloc(limit*sizeof(int));
	dgesv_(&n,&n,b,&n,ipiv,ident,&n,&info);
	for(i = 0; i < n; ++i){
		for(j = 0; j < n; ++j){
			printf("%16.6g ", IDENT(i,j));
		}
		printf("\n");
	}
	
	/*
 	* Now check it against dgemm
 	*/
	double alpha=1.0, beta=0.0;
        char trans = 'N';
        printf("B inverse * B:\n");
        double *c=malloc(sizeof(double)*n*n);
        dgemm(&trans, &trans, &n, &n, &n, &alpha, ident, &n, bcopy, &n, &beta, c, &n);
        for(i=0;i<n;++i){
                for(j=0;j<n;++j)
                        printf("%16.6g ", C(i,j));
                printf("\n");
        }


	/* 
 	*
 	* Weighted sum weights
 	*/
	printf("\nWeighted sum interpolation\n");
	double weights[8];
	double testval = 0.5;
	for(j = 0; j < n; j++){
		double sum = 0.0;
		for(i = 0; i < n; i++){
			sum = sum + (IDENT(j,i) * (pow(testval,i))) ;			
		}
		weights[j] = sum;
		printf("w%d : %f\n",j,weights[j]); 	
	}
	//get analytical f val
	double final = 0;
	for(j = 0; j < n; j++){
		final = final + (weights[j] * bcopy(7,j));
	}
	printf("\nx %9.9s numerical %9.9s analytical %9.9s abs error|num - analyitcal|\n","","","");
	printf("%9.9f %9.9f %18.9f %24.9f\n", testval, pow(testval,7),final,fabs(pow(testval,7) - final));
	

	/*
	* First order differentiation
	*
	*/
        printf("\nFirst order differentiation\n");
	testval = 0.5;
        for(j = 0; j < n; j++){
                double sum = 0.0;
                for(i = 0; i < n; i++){
                        sum = sum + (IDENT(j,i) * i*(pow(testval,i-1))) ;
                }
                weights[j] = sum;
                printf("w%d : %f\n",j,weights[j]);
        }	

        //get analytical f val
        final = 0;
        for(j = 0; j < n; j++){
                final = final + (weights[j] * bcopy(7,j));
        }
        printf("\nx %9.9s numerical %9.9s analytical %9.9s abs error|num - analyitcal|\n","","","");
        printf("%9.9f %9.9f %18.9f %24.9f\n", testval, 7*pow(testval,6),final,fabs(7*pow(testval,6) - final));

        /*
 	*Second order differentiation
 	*
        */
	printf("\nSecond order differentiation\n");
        testval = 0.5;
        for(j = 0; j < n; j++){
                double sum = 0.0;
                for(i = 0; i < n; i++){
                        sum = sum + (IDENT(j,i) * i*(i-1)*(pow(testval,i-2))) ;
                }
                weights[j] = sum;
                printf("w%d : %f\n",j,weights[j]);
        }

        //get analytical f val
        final = 0;
        for(j = 0; j < n; j++){
                final = final + (weights[j] * bcopy(7,j));
        }
        printf("\nx %9.9s numerical %9.9s analytical %9.9s abs error|num - analyitcal|\n","","","");
        printf("%9.9f %9.9f %18.9f %24.9f\n", testval, 7*6*pow(testval,5),final,fabs(7*6*pow(testval,5) - final));

	
	/*
 	* Integration method
 	*/
	printf("\nIntegration method\n");
	testval = 0.5;
        for(j = 0; j < n; j++){
                double sum = 0.0;
                for(i = 0; i < n; i++){
                        sum = sum + (IDENT(j,i) * (pow(testval,i+1)/(i+1)));
                }
                weights[j] = sum;
                printf("w%d : %f\n",j,weights[j]);
        }

        //get analytical f val
        final = 0;
        for(j = 0; j < n; j++){
                final = final + (weights[j] * bcopy(7,j));
        }
        printf("\nx %9.9s numerical %9.9s analytical %9.9s abs error|num - analyitcal|\n","","","");
        printf("%9.9f %9.9f %18.9f %24.9f\n", testval, pow(testval,8)/8,final,fabs(pow(testval,8)/8 - final));


	free(b);
	free(ident);
	return bcopy;
}

int main(int argc, char** argv){
	getBMat();
	return 0;
}
