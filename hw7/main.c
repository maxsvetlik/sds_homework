#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#define A(i,j) a[(i*n) + (j)]


void jacobi(double* a, double* b){
	int n = 3;
	
	/*
 	* Begin jacobi algorithm
 	*/
	double x[3];
	double xold[3];
	
	//choose initial guess x[0]
	x[0] = 11.9988222;x[1] = 3.100033; x[2] = -0.003;
	
	int limit = 10000;
	double sigma;
	int i,j,k = 0;
	double epsilon = 1.0E-39;
	char converged = 0;
	
	while(k < limit && !converged){
		xold[0] = x[0]; xold[1] = x[1]; xold[2] = x[2];
		for(i = 0; i < n; i++){
			sigma = 0;
			for(j = 0; j < n; j++){
				if(i != j){
					sigma = sigma+(A(i,j) * x[j]);
				}
			}
			x[i] = (b[i] - sigma)/A(i,i);
		}
		double x_sum = 0;
		double xold_sum = 0;
		for(i = 0; i < n; i++){
			x_sum += x[i];
			xold_sum += xold[i];
			//printf("%.9ef ",x[i]);
		}
		//printf("\n");
		if(fabs((x_sum - xold_sum))/fabs(xold_sum) < epsilon)
			converged = 1;
		k++;
	}
	printf("Final x: after %d iterations for the Jacobi method\n",k);
	printf("%.16e\n",x[0]);
	printf("%.16e\n",x[1]);
	printf("%.16e\n",x[2]);
}

void gauss(double* a, double* b){

	/*
 	* Begin Gauss-Seidel alg
 	*/
	int i,j,n=3;
	double* x = malloc(sizeof(double)*n);
	char converged = 0;
	double sigma;
	double *theta = malloc(sizeof(double)*n);
	double epsilon = 1.0E-39;
	
	//set initial guess
	theta[0] = 11.9988222; theta[1] = 3.100033; theta[2] = -0.003;
	//theta[0] = 0; theta[1] = 0; theta[2] = 0;
	
	double old_sum = 0.001,cur_sum = 0.001;
	int k = 0;
	int limit = 100000; //limit in the case of no-convergence
	
	while(k < limit && !converged){
		for(i = 0; i < n; i++){
			sigma = 0;
			for(j = 0; j < n; j++){
				if(j != i)
					sigma = sigma + A(i,j)*theta[j];
			}
			theta[i] = (b[i] - sigma)/A(i,i);
		}
		old_sum = cur_sum;
		for(i = 0; i < n; i++){
			cur_sum += theta[i];
		}
		if(fabs(cur_sum - old_sum)/old_sum < epsilon)
			converged = 1;
		k++;
	}
        printf("Final theta: for the Gauss-Seidel method\n");
        printf("%.16e\n",theta[0]);
        printf("%.16e\n",theta[1]);
        printf("%.16e\n",theta[2]);

}

int main(int argc, char** argv){
	/*
 	* Fill mat
 	*/
	int n = 3;
	double* a = malloc(sizeof(double)*n*n);
	double* b = malloc(sizeof(double)*n);
	b[0] = 12.0; b[1] = 27.0; b[2] = 36.0;
	/*
 	* Assigned Matrix A
 	*/
	A(0,0) = 1.0; A(0,1) = 1.0; A(0,2) = 2.0;
	A(1,0) = 2.0; A(1,1) = 3.0; A(1,2) = 5.0;
	A(2,0) = 3.0; A(2,1) = 3.0; A(2,2) = 6.0;
	
	/*
 	* Created Matrix A - > strictly diagonally dominant
 	*/
        /*
	A(0,0) = 4.0; A(0,1) = 1.0; A(0,2) = 2.0;
        A(1,0) = 2.0; A(1,1) = 5.0; A(1,2) = 2.0;
        A(2,0) = 3.0; A(2,1) = 3.0; A(2,2) = 7.0;
	*/
	jacobi(a,b);
	gauss(a,b);
	free(a);
	free(b);
	return 0;
}	

