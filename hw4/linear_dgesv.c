#include <stdlib.h>
#include <stdio.h>
#define A(i,j) a[(i) + (j)*n]

void dgetrf_();
void dgetrs_();
void dgesv_();
/*
 * Part 1, solve Ax = b
 */
int solve_linear_eq(){
	int n = 3;
	double *a = malloc(n*n*sizeof(double));
	double *b = malloc(n*sizeof(double));
	int i,j,one=1,info;
	int *ipiv=malloc(n*sizeof(int));
	char trans[1]={'N'};
	A(0,0)=6.;  A(0,1)=-2.;  A(0,2)=2.; A(1,0)=12.;
	A(1,1)=-8.;  A(1,2)=6.; A(2,0)=3.;  A(2,1)=-13.;
	A(2,2)=3.; b[0]=16.;   b[1]=26.;   b[2]=-19.;
	dgesv_(&n,&one,a,&n,ipiv,b,&n,&info);
	for(j=0;j<n;++j){
		printf("%f\n", b[j]);
	}	
	return 0;
}


int main(int argc, char** argv){
	solve_linear_eq();
	return 0;
}
