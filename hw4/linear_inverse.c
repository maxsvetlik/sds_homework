#include <stdlib.h>
#include <stdio.h>
#define A(i,j) a[(i) + (j)*n]
#define B(i,j) b[(i) + (j)*n]
#define acopy(i,j) acopy[(i) + (j)*n]
#define C(i,j) c[(i) + (j)*n]

void dgemm_();
void dgesv_();

void make_inverse(){
	int i,j,n = 5;
	double *acopy = malloc(sizeof(double)*n*n);
	double *a = malloc(sizeof(double)*n*n);
	double *b = malloc(sizeof(double)*n*n);
	double initValue = 1.;
	printf("Original A:\n");
	for(i = 0; i < n; ++i){
		for(j = 0; j < n; ++j){
			acopy(i,j) = initValue/(i+j+1);
			A(i,j) = initValue/(i+j+1);
			printf("%16.6g ",A(i,j));
			if(i == j)
				B(i,j) = 1;
			else
				B(i,j) = 0;
		}
		printf("\n");
	}
	printf("\nComputed A inverse:\n");
	int five=5,info,one=1;
	int *ipiv=malloc(n*sizeof(int));
	dgesv_(&n,&n,a,&n,ipiv,b,&n,&info);
	for(i =0;i<n;++i){
		for(j=0;j<n;++j){
			printf("%16.6g ", B(i,j));
		}
		printf("\n");
	}

	/*
 	* Now check it against dgemm
 	*/
	double alpha=1.0, beta=0.0;
	char trans = 'N';
	printf("A inverse * A:\n");
	double *c=malloc(sizeof(double)*n*n);
	dgemm(&trans, &trans, &n, &n, &n, &alpha, b, &n, acopy, &n, &beta, c, &n);
	for(i=0;i<n;++i){
		for(j=0;j<n;++j)
			printf("%16.6g ", C(i,j));
		printf("\n");
	}

	printf("A * A inverse:\n");
	dgemm(&trans, &trans, &n, &n, &n, &alpha, acopy, &n, b, &n, &beta, c, &n);
	for(i = 0;i<n;++i){
		for(j=0;j<n;++j)
			printf("%16.6g ", C(i,j));
		printf("\n");
	}
	free(a);
	free(b);
	free(acopy);
	free(c);
}

int main(int argc, char** argv){
	make_inverse();
}
