/*
 * Outputs a file rand_exp.txt filled with n numbers on [0,inf)
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double myrandom(){
	return (rand()+1.)/(RAND_MAX + 1.);
}

int main(int argc, char** argv){
	time_t t;
	srand((unsigned) time(&t));
	
	int i;
	int n = 100000;
	double red = 0.;
	double total = 0.;
	int limit = 10;
	double r = 0.;
	for(i = 0; i < n; i++){
		double rand1 = myrandom();
		double rand2 = myrandom();
		r = sqrt(rand1*rand1 + rand2*rand2);
		
		if(r < 1){
			red++;
		}
		total++;
		if(i == limit){
			printf("%d %f\n",limit, 4*red/total);
			limit*= 10;
		}
	}
	return 0;
}
