/*
 * Outputs a file rand_exp.txt filled with n numbers on [0,inf)
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char** argv){
	time_t t;
	FILE *f = fopen("rand_exp.dat", "w");
	double lambda = .333333333333;
	/* Intializes random number generator */
	srand((unsigned) time(&t));
	int i;
	int n = 100000;
	for(i = 0; i < n; i++){
		double random = (double)rand() / (double)RAND_MAX ;
		double printval = 0.0;
		printval = -logf(1.0f - random) / lambda;
		fprintf(f,"%f\n", printval);
	}
	fclose(f);
	return 0;
}
