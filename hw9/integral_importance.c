/*
 * Integrates on (-5, 5) with importance sampling
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double myrandom(){
	return (rand()+1.)/(RAND_MAX +1.);
}

int main(int argc, char** argv){
	time_t t;
	srand((unsigned) time(&t));
	
	int i,k;
	int n[5] = {10,100,1000,10000,100000};
	double red = 0.;
	double total = 0.;
	int limit = 10;
	double r = 0.;
	for(k = 0; k < 5; k++){
		int points = 0;
		for(i = 0; points < n[k]; i++){
			double x = myrandom();
			double y = myrandom();
			y = -5 + 10.0*y;
			if(x <= exp(-y * y/2.)){
				r +=  (5.0+cos(y));
				points++;
			}
		}	
		printf("%d %f\n",n[k], (sqrt(2*3.1459)*r/n[k]));
		r = 0.;
	}
	return 0;
}
