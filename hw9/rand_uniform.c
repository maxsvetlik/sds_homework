/*
 * Outputs a file rand_uniform.txt filled with n numbers on [-5,5]
 *
 */

#include <stdlib.h>
#include <stdio.h>


int main(int argc, char** argv){
	time_t t;
	FILE *f = fopen("rand_uniform.dat", "w");
	/* Intializes random number generator */
	srand((unsigned) time(&t));	
	int i;
	int n = 100000;
	for(i = 0; i < n; i++){
		fprintf(f,"%d\n", rand() % 10 -5);
	}
	fclose(f);
	return 0;
}
