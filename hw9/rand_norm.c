/*
 * Outputs a file rand_exp.txt filled with n numbers on [0,inf)
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double myrandom(){
	return (rand()+1.)/(RAND_MAX + 1.);
}

int main(int argc, char** argv){
	time_t t;
	FILE *f = fopen("rand_norm.dat", "w");
	srand((unsigned) time(&t));
	
	int i;
	int n = 100000;
	double num_x = 0.;
	double x_num_x = 0.;
	int points = 0;
	for(i = 0; points < n; i++){
		double rand1 = myrandom();
		double rand2 = myrandom();
		rand2 = -5. + 10.0*rand2;
		
		if(rand1 <= exp(-rand2 * rand2/2.)){
			x_num_x = rand2;
			fprintf(f,"%f\n", x_num_x);
			points++;
		}
	}
	fclose(f);
	return 0;
}
