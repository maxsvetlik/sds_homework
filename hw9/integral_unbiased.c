/*
 * Integrates on (-5, 5)
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

double myrandom(){
	return (rand() % 10 -5);
}

int main(int argc, char** argv){
	time_t t;
	srand((unsigned) time(&t));
	
	int i,k;
	int n[5] = {10,100,1000,10000,100000};
	double red = 0.;
	double total = 0.;
	int limit = 10;
	double r = 0.;
	for(k = 0; k < 5; k++){
		for(i = 0; i < n[k]; i++){
			double x = myrandom();
			r +=  exp(-x*x/2.0)*(5.0+cos(x));
		}	
		printf("%d %f\n",n[k], (10*r/n[k]));
		r = 0.;
	}
	return 0;
}
