#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#define n 3

/*
 * Computes the dotproduct of two vectors
 */
double dot(double* u, double* v){
	double result = 0.;
	int i = 0;
	for(i = 0; i < n; i++)
		result = result + u[i]*v[i];
	return result;
}

/*
 * Computes the projection of x onto z
 */
double* project(double* x, double* z){
	double upper = dot(x,z);
	double lower = dot(z,z);
	int i;
	double scale = upper/lower;
	/*
	* Now scale z by the scaled factor
	*/
	//static double * temp;
	double * temp = malloc(sizeof(double) * n);
	for(i = 0; i < n; i++){
		temp[i] = z[i] * scale;
	}
	return temp;
}
/*
 * Normalizes vector x, stuffs answer into buff
 */
void normalize(double* x, double* buff){
	double norm = dot(x,x);
	norm = sqrt(norm);
	int i;
	for(i = 0; i < n; i++){
		buff[i] = x[i]/norm;
	}
}
void mgs(){
	/*double a[n][n] = {
	{1., 2., 1.},
	{0., 1., 2.},
	{1., 2., 0.}
	};
	*/
	double a[n][n] = {
	{1.,0.,1.},
	{2.,1.,2.},
	{1.,2.,0.}
	};
	int i,j,k = 0;
	double q[n][n];
	double *r;
	for(i = 0; i < n; i++){
		normalize(&a[i][0], &a[i][0]);
		for(j = i+1; j < n; j++){
			r = project(&a[j][0],&a[i][0]);
			for(k = 0; k < n; k++){
				a[j][k] = a[j][k] - r[k];
			}
			free(r);			
		}
	}
	
	printf("\nOrthogonal basis after running Modified Gram Schmidt:\n\n");
	for(i = 0; i < n; i++){
		for(j = 0; j < n; j++)
			printf("%.16e  ",a[i][j]);
		printf("\n");
	}
}
void cgs(){
	double a[n][n] = {
        {1.,0.,1.},
        {2.,1.,2.},
        {1.,2.,0.}
        };
        int i,j,k = 0;
        double v[n][n];
	double q[n][n];
        double r[n][n]; 


	for(j = 0; j < n; j++){
		for(k = 0; k < n; k++)
			v[j][k] = a[j][k];
		for(i = 0; i < j; i++){
			r[i][j] = dot(&q[i][0], &a[j][0]);
			for(k = 0; k < n; k++)
				v[j][k] = v[j][k] - r[i][j]*q[i][k];
		}
		normalize(&v[j][0], &q[j][0]);
	}
      	
	printf("\nOrthogonal basis after running Classical Gram Schmidt:\n");
        for(i = 0; i < n; i++){
                for(j = 0; j < n; j++)
                        printf("%.16e  ",q[i][j]);
                printf("\n");
        }
}

int main(int argc, char** argv){
	mgs();
	cgs();
	return 0;
}	

