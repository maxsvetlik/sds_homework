#include "parameters.h"
//#include <stdio.h>
#include <time.h>
#include <sys/types.h>
int main(int argc, char** argv){
	double startx = 0;
	double endx = 1;
	const double actual = 0.125;
	int nlist[7] = {11, 101, 1001, 10001, 100001,  1000001, 10000001};
	int i,j;
	double mid_sum = 0, trap_sum = 0, simp_sum = 0;
	long long unsigned int mid_time = 0, trap_time = 0, simp_time = 0;
	struct timespec start,end;

	printf("%10s  %15s  %15s %15s %15s %15s %15s %20s %20s %20s","M", "midp", "trap", "simp","midp_err", "trap_err","simp_err", "mid_time/error", "trap_time/error", "simp_time/error\n");
	for(i = 0; i < 7; i=i+1){
		int m = nlist[i];
		double h = (endx - startx) / (m-1);
		mid_sum = 0;
		trap_sum = 0;
		simp_sum = 0;		
		mid_time = 0;
		trap_time = 0;
		simp_time = 0;
		for(j = 0; j < m; j=j+1){
			double xi = startx + h*j;
			clock_gettime(CLOCK_MONOTONIC, &start);
			mid_sum = mid_sum + midp(xi, h);
			clock_gettime(CLOCK_MONOTONIC, &end);		
			mid_time += (double)(end.tv_sec - start.tv_sec + end.tv_nsec - start.tv_nsec);		
			clock_gettime(CLOCK_MONOTONIC, &start);
			trap_sum = trap_sum + trap(xi, h);
			clock_gettime(CLOCK_MONOTONIC, &end);
			trap_time += (double)(end.tv_sec - start.tv_sec + end.tv_nsec - start.tv_nsec); 
			clock_gettime(CLOCK_MONOTONIC, &start);
			simp_sum = simp_sum + simp(xi, h);
			clock_gettime(CLOCK_MONOTONIC, &end);

			simp_time += (double)(end.tv_sec - start.tv_sec + end.tv_nsec - start.tv_nsec);
		}
		mid_sum = mid_sum * h;
		trap_sum = trap_sum * h;
		simp_sum = simp_sum * h;
		
		printf("%10ld  %15.13f  %15.13f  %15.13f  %15.13f  %15.13f  %15.13f %20f %20f %20f\n", m, mid_sum, trap_sum, simp_sum, actual-mid_sum, actual-trap_sum, actual-simp_sum, (double)abs(actual-mid_sum)/mid_time,(double)abs(actual-trap_sum)/trap_time,(double)abs(actual-simp_sum)/simp_time);
	}
	return 0;
}
